void move(int grid[], int* nrc, int* tot, int* rob, int* moved)
{
  int trygrid[*tot], mkg[*tot], mkginv[*tot];
  int RM = *tot - *nrc;
  //construct trygrid
  for(int i = 0; i < *nrc; i++)
    trygrid[i] = grid[RM+i];

  for(int j = 0; j < RM; j++)
    trygrid[j+*nrc] = grid[j];

  //construct mkg and update part of grid
  for(int k = 0; k < *tot; k++)
  {
    mkg[k] = trygrid[k] - grid[k];

    if(mkg[k] == *rob)
    {
      grid[k] = *rob + 1;
      *moved = *moved + 1;
    }
  }

  //construct mkginv from mkg
  for(int l = 0; l < RM; l++)
    mkginv[l] = mkg[*nrc+l];

  for(int m = 0; m < *nrc; m++)
    mkginv[RM+m] = mkg[m];

  //update the rest of grid
  for(int n = 0; n < *tot; n++)
  {
    if(mkginv[n] == *rob)
      grid[n] = 1;
  }

}




