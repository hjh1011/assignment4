\name{pmove}
\alias{pmove}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
Output the proportion of moved cars in each step
}
\description{
Calculate the proportion of moved cars in each steps
}
\usage{
pmove(grid, steps)
}

\arguments{
  \item{grid}{
a BML object
}
  \item{steps}{
a number of steps that the grid moves
}
}

\value{
It returns a vector with a length equal to "steps".
}


\examples{
GD = createBMLGrid(5, 5, 0.3)
pmove(GD, 10)
}

