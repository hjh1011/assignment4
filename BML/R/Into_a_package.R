#####My best
#####Generate color matrix
createBMLGrid = function(nr, nc, rho, bnr = c(1, 1))
{
  if(nr<=0 | nc<=0 | rho<= 0 | bnr[1]<=0 | bnr[2]<=0)
    stop('nr, nc, rho, bnr must be positive!')
  
  if(rho>1)
    stop('rho cannot be larger then 1')
  
  pblue = bnr[1]/sum(bnr)
  pred = bnr[2]/sum(bnr)
  smp = sample(c(1L, 5L, 10L), nr*nc, replace = TRUE, prob = c(1 - rho, rho*pblue, rho*pred))
  moved = c(0L, 0L)
  names(moved) = c('blue', 'red')
  grid = list(mtrx = matrix(smp, nr), step = 0, moved = moved)
  class(grid) = c(class(grid), 'BML')  ##return a BML class object
    
  return(grid)
} ##In grid$mtrx,  1 is for empty, 5 is for blue and 10 is for red


plot.BML = function(x,...)
{
  DM = dim(x$mtrx)
  image(1:DM[2], 1:DM[1], t(x$mtrx), col = c('white', 'blue', 'red'),
        xlab = DM[2], ylab = DM[1], axes = FALSE, main = paste0('Step: ', x$step))
} ##for plotting


moveonestep = function(grid, nr, nc)
{
  ##move blue cars
  bluemove = grid$mtrx[c(nr, 1:(nr-1)), ]  ##for here, "up is down"
  bmvto = (grid$mtrx - bluemove) == -4  ##the grids where those blue cars can move to
  bmvfrom = bmvto[c(2:nr, 1), ]  ##the grids where those blue cars move from
  grid$mtrx[bmvto] = 5
  grid$mtrx[bmvfrom] = 1
  ##move red cars
  redmove = grid$mtrx[, c(nc, 1:(nc-1))]
  rmvto = (grid$mtrx - redmove) == -9
  rmvfrom = rmvto[, c(2:nc, 1)]
  grid$mtrx[rmvto] = 10
  grid$mtrx[rmvfrom] = 1
  ##increment step
  grid$step = grid$step + 1
  ##record numbers of moved blue cars and red cars
  grid$moved[1] = sum(bmvto)
  grid$moved[2] = sum(rmvto)
  
  grid
}  ##move one step(both red and blue)


runBMLGrid = function(grid, steps)
{
  if(all(class(grid) != 'BML'))
    stop('grid must be a BML object!')
  
  if(steps < 1)
    stop('steps must be larger or equal to 1')
  
  DM = dim(grid$mtrx)
  for(i in 1:steps)
  {
    if(sum(grid$moved) == 0 & grid$step != 0) ##already jamed
    {
      grid$step = steps
      break   ##break when no car can move!!!
    }
    
    else
      grid = moveonestep(grid, DM[1], DM[2])
  }
  
  return(grid)
} 

summary.BML = function(object,...)
{
  nblue = sum(object$mtrx==5)
  nred = sum(object$mtrx==10)
  
  smmy = c(nblue, nred)
  names(smmy) = c('number of blue cars', 'number of red cars')
  
  smmy
}

pmove = function(grid, steps)
{
  if(all(class(grid) != 'BML'))
    stop('grid must be a BML object!')
  
  if(steps < 1)
    stop('steps must be larger or equal to 1')
  
  mvdcars = numeric(steps)
  ncars = sum(summary(grid))
  for(i in 1:steps)
  {
    grid = runBMLGrid(grid, 1)
    mvdcars[i] = sum(grid$moved)
  }
  
  mvdcars/ncars
}

##########Cgrid

cmoveonestep = function(grid, nr, nc)
{
  tot = as.integer(nr*nc)
  ##move blue cars
  tmp = .C('move', t(grid$mtrx), nc, tot, 4L, 0L)
  grid$moved[1] = tmp[[5]]
  ##move red cars
  tmp = .C('move', t(tmp[[1]]), nr, tot, 9L, 0L)
  grid$moved[2] = tmp[[5]]
  ##increment step
  grid$step = grid$step + 1
  grid$mtrx = tmp[[1]]
  
  return(grid)
}

crunBMLGrid = function(grid, steps)
{
  if(all(class(grid) != 'BML'))
    stop('grid must be a BML object!')
  
  if(steps < 1)
    stop('steps must be larger or equal to 1')
  
  DM = as.integer(dim(grid$mtrx))
  for(i in 1:steps)
  {
    if(sum(grid$moved) == 0 & grid$step != 0) ##already jamed
    {
      grid$step = steps
      break
    }
    
    else
      grid = cmoveonestep(grid, DM[1], DM[2])
  }
  
  return(grid)
}

cpmove = function(grid, steps)
{
  if(all(class(grid) != 'BML'))
    stop('grid must be a BML object!')
  
  if(steps < 1)
    stop('steps must be larger or equal to 1')
  
  mvdcars = numeric(steps)
  ncars = sum(summary(grid))
  for(i in 1:steps)
  {
    grid = crunBMLGrid(grid, 1)
    mvdcars[i] = sum(grid$moved)
  }
  
  mvdcars/ncars
}







